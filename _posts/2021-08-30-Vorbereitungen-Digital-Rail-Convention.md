---
layout: post
title:  "Vorbereitungen auf die Digital Rail Convention"
date:   2021-08-30 11:13:16 -0100
---

Wir werden auf dem Demo Day der [Digital Rail Convention 2021](https://www.smart-rail.cc/de/Digital-Rail-Convention) unsere Ergebnisse präsentieren können. Wir sind in den Messeständen bei den mFund Projekten vor Ort und am Do. den 09.09.2021 werden wir die Blockchain Einbauten im Zug präsentieren. **Kommen Sie vorbei!**
