---
layout: post
title:  "Besuch des Advanced Train Lab"
date:   2020-06-10 10:00:14 -0100
# categories: jekyll update
---

Ein *Higlight* im Projekt ist die Erprobung der praktischen Anwendung. Dazu sind wir eine Kooperation mit dem [Advanced Train Lab](https://www.deutschebahn.com/de/Digitalisierung/technologie/advanced-TrainLab-das-schnellste-Labor-auf-Schienen-3953074) eingegangen, um die Herausforderung anuzunehmen unsere im Labor entwickelten Komponenten und Prinzipien auf einem fahrenden Zug einzusetzen. 

Wir haben die freundliche Einladung der Projektmannschaft des Testzuges angenommen, um den Zug einmal life zu erleben und erste Sondierungen über den Einbauort der Komponenten abzustimmen. Dieses Treffen fand selbstverständlich unter Einhaltung des vorgegebenen Hygienekonzeptes statt. 

Anbei ein Beitrag, der beim Besuch entstanden ist:
<p>
<!--
    <video width="100%" controls preload="auto" src="https://railchain-media.beta.de.com/stream/railchain-adv-2020-hd-v2.mp4"></video>
-->
<iframe  width="790" height="444"  src="https://www.youtube.com/embed/s5NXyRAEN7I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>
