---
layout: page
title: Referenzimplementierung, Aufbau für Labortests 
permalink: /referenzimp/
hide_from_menu: true
---

Die Referenzimplementierung für Laboruntersuchungen entstand aus einer Kooperation von Mitarbeitern der TU Braunschweig und der Siemens Mobility GmbH. 
Die Ergebnisse sind in der Publikation ["ZugChain: Blockchain-Based Juridical Data Recording in Railway Systems"](https://ieeexplore.ieee.org/abstract/document/9833566) veröffentlicht und der entstandene [Sourcecode](https://github.com/ibr-ds/zugchain) ist ebenfalls verfügbar.
Das folgende Abbild stellt die wesentlichen Komponenten des Systems dar: 

<p style="text-align: center">
    <img src="../images/Railchain_TUBS_Train.png" alt="Referenzimplementierung_Architektur" width="500px" />
</p>

Die Funktion des Konsenzmechanismus als Herzstück des Systems ist im folgenden Bild dargestellt: 

<p style="text-align: center">
    <img src="../images/Railchain_TUBS_Consensus.png" alt="Konsenzverhalten" width="500px" />
</p>


Ziel des Aufbaues war eine Laborumgebung zu schaffen, die möglichst nah an eine reale Situation innerhalb eines Zuges aus Sicht einer JRU heranreichen konnte:
<p style="text-align: center">
    <img src="../images/Railchain_MCOM.png" alt="Referenzimplementierung" width="500px" />
</p>

Das Sytem besteht aus den folgenden Komponenten:
- eine Datenquelle für die Zugdaten (DDC)
- ein MVB Bus, der die Testdaten transportiert (MVB Master)
- 4 Rechner, die die MVB Daten empfangen und den Konsenzmechanismus abwickeln (M-COM's)
- ein Netzwerk auf dem der Konsenz gebildet werden kann (Switch)
- eine handelsüblche JRU als Referenzwertgeber (JRU)
- eine Steuereinheit und Netzwerkbrücke (PI)  

Dieses System kann heran gezogen werden, um:
- das normale Verhalten des Konsenzmechanismus zu testen und zu optimieren
- Fehlersituationen und Ausfälle zu simulieren

Die Abbildung des Systemzustandes erfolgt über eon Dashboard, das die wesentlichen Informationen enthält: 
![Raily_Dashboard](../images/Railchain_TUBS_Dashboard.png)

Besonderer Dank an dieser Stelle geht an:

die Mitarbeiter des [Institutes für Betriebssysteme und Rechnerverbund der TU Braunschweig](https://www.ibr.cs.tu-bs.de/ds/): Prof. Dr. Rüdiger Kapitza und die wissenschaftlichen Mitarbeiter Signe Rüsch, Kai Bleeke und Ines Messardi

Die Mitarbeiter der [Siemens Mobility GmbH](https://new.siemens.com/global/de/produkte/mobilitaet.html):
Dr. Jerns Braband, Dr. Matthias Müller, Katharina Olze, Stephan Griebel und Robert Grosser