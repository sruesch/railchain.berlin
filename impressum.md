---
layout: page
title: Impressum und Datenschutzerklärung
permalink: /impressum/
hide_from_menu: true
---

- [Impressum](https://osm.hpi.de/legals#impressum)
- [Datenschutzerklärung]({{ '/datenschutz' | prepend: site.baseurl }})
