---
layout: page
title: Datenschutz
permalink: /datenschutz/
hide_from_menu: true
---

Diese Webseite wird über das Hosting von *GitLab Pages* angeboten.
Entsprechend sind Details zum Datenschutz den Dokumenten
[privacy policy](https://about.gitlab.com/privacy/) und
[privacy compliance](https://about.gitlab.com/privacy/privacy-compliance/)
zu entnehmen.
