---
layout: page
title: Asset Identity und Asset Tracking
permalink: /asset_identity/
hide_from_menu: true
---

Der Use Case Asset Identity untersucht die Entwicklung von vertrauenswürdigen, blockchain-basierten Identitäten für Assets, die ein ganzes Triebfahrzeug oder eine intelligente Komponente entsprechen kann. Dier Anwendungen dafür sind vielfältig:
- eindeutige Identifier sorgen über Unternehmens- und Ländergrenzen hinweg für eine klare Identifikation einer Komponente. Eine saubere Historie für jede wesentliche Komponente sind die Grundlage für weitreichende Prozessverbesserungen
- kryptografische Signaturen können die Integrität und Herkunft von Informationen nachweisbar absichern, gerade bei Garantie oder Compliance Anforderungen unabdingbare Funktionen
- Standards über die feingranulare Berechtigung mit wem Assets kommunizieren dürfen und was Ihnen erlaubt wird zu beeinflussen eröffnen weitreichende Interoperabilität und Absicherung

Der vom W3C entwickelte Standard [Decentralized Identifiers (DID)](https://www.w3.org/TR/did-core/) bietet den vorher geforderten Funktionsumfang und dieser Use-Case des Forschungsprojekt erprobt die Anwendung des Standards auf Basis von Open Source Komponenten , die die Referenzimplementierung des Standards darstellen. 

Eine Vorraussetzung für die Anwendung von DID's ist, dass die beteiligten Komponenten die folgenden Vorraussetzungen mitbringen:  
- Kommunikation mit einem IP Netzwerk 
- Durchführung von kryptographischen Operationen mit asymmetrischen Schlüsseln zum prüfen und erzeugen einer Signatur
Im Bahnbetrieb erfüllen sehr viele Steuergeräte die Anforderungen und würden sich für eine Anwendung eignen.

Die am Markt übliche Technologie der X.509 Zertifikate zur kryptografischen Absicherung von Kommunikation und Identität hat die Schwäche eine zentrale Certification Authority zu benötigen, die lediglich durch Signaturen die Authentizität von Zertifikaten absichert. Der DID Standard beruht auf einem Registrierungssystem, dass durch ein Blockchain-Netzwerk bereitgestellt wird. Jeder einzelne des Betreibergremiums dieses Netzwerkes steht für die Integrität und Authentizität der darin enthaltenen Informationen. Das Railchain Projekt ist eine Kooperation mit dem [IDunion](https://idunion.org/) Netzwerk eingegangen, um auf Basis eines verteilten Netzwerkes evaluieren zu können:      
![IDunion decentralized Identifier Network](../images/Railchain_AssetId_IDUnion.png)

Die Kommunikation zwischen den Beteiligten (das sind die Firmen, denen Assets gehören) wird abgesichert durch eine Registrierung des öffentlichen Schlüsselmaterials unter der eindeutigen DID des Interessenten:
![Regsitrierungsvorgang an dem IDunion DID Netzwerk](../images/Railchain_AssetId_Registrierung.png)

 Assets selbst müssen nicht registriert werden, sie haben Identitäten nach demselben Muster, aber diese werden legitimiert durch ausgestellte Zertifikate der registrierten Teilnehmer. Der folgende Artikel beschreibt die Zusammenhänge genauer: [Trusted P2P Messaging with DID's](https://medium.com/uport trusted-p2p-messaging-with-dids-didcomm-and-vcs-398f4c3f3cda). 

Die Prozesse können und sollten so gestaltet werden, dass im Netzwerk nur öffentlich zugängliche Informationen gespeichert werden. Der Austausch von sensiblen Daten erfolgt über abgesicherte Kommunikationskanäle direkt zwischen den Beteiligten und den Assets. Die Methodik sorgt für Vertrauen in die Integrität und Herkunft der Daten, so dass bei personenbezogenen Informationen die DSGVO recht einfach einzuhalten ist. 

Die reine Registrierung reicht noch nicht aus. Sie kann lediglich beweisen, dass eine kryptografische Signatur einer registrierten DID den passenden privaten Schlüssel der zugehörigen DID erzeugt wurde. Ein sicherer Kommunikationskanal entsteht erst, wenn zwischen den Beteiligten ein neuer Kanal eröffnet wird und ein Austausch der DID's erfolgt. Durch diesen Schritt, der eine beidseitige aktive Interaktion erfordert, wissen beide Partner wem die registrierte DID zugeordnet werden kann:
![Vertrauen schaffen!](../images/Railchain_AssetId_Vertrauen_schaffen.png)

Erst jetzt sind die Vorraussetungen geschaffen, um die folgenden für den Bahnbetrieb interessanten Szenarien umzusetzen:

[Ausstellen eines Typenschildes für ein Asset]({{ '/asset_id_Typenschild' | prepend: site.baseurl }})

[Wechsel des Besitzers eines Assets]({{ '/asset_id_Besitzerwechsel' | prepend: site.baseurl }})

[Legitimierte Wartung durch einen Partner]({{ '/asset_id_Partnerwartung' | prepend: site.baseurl }})

Besonderer Dank an dieser Stelle geht an die Siemens Mitarbeiter:

Marquart Franz, Carlos Morra und den Master Studenten Stephan Penner 